#!/usr/bin/env python
from gi.repository import Gtk


class Handlers:
    def __init__(self):
        pass

    def main_window_close(self, *args):
        Gtk.main_quit(*args)

    def test_button_clicked_cb(self, *args):
        window = args[0]
        window.show_all()

    def test_window_cancel_cb(self, *args):
        window = args[0]
        window.hide()


def setup_assistants(builder):
    def setup_assitant(a):
        first_page_widget = a.get_nth_page(0)
        a.set_page_complete(first_page_widget, True)

        last_page_widget = a.get_nth_page(-1)
        a.set_page_type(last_page_widget, Gtk.AssistantPageType.SUMMARY)

    setup_assitant(builder.get_object("power_board_window"))
    setup_assitant(builder.get_object("motor_board_window"))
    setup_assitant(builder.get_object("servo_board_window"))
    setup_assitant(builder.get_object("ruggeduino_window"))


def main():
    builder = Gtk.Builder()
    builder.add_from_file("gui_assets/testing_rig_gui.glade")
    builder.connect_signals(Handlers())

    setup_assistants(builder)

    window = builder.get_object("main_window")
    window.show_all()

    Gtk.main()

if __name__ == "__main__":
    main()